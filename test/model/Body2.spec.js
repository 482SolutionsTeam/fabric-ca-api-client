/*
 * Electrodo CA
 * Specification for REST API Electrodo CA
 *
 * OpenAPI spec version: 1.0.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.15
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ElectrodoCa);
  }
}(this, function(expect, ElectrodoCa) {
  'use strict';

  var instance;

  describe('(package)', function() {
    describe('Body2', function() {
      beforeEach(function() {
        instance = new ElectrodoCa.Body2();
      });

      it('should create an instance of Body2', function() {
        // TODO: update the code to test Body2
        expect(instance).to.be.a(ElectrodoCa.Body2);
      });

      it('should have the property status (base name: "status")', function() {
        // TODO: update the code to test the property status
        expect(instance).to.have.property('status');
        // expect(instance.status).to.be(expectedValueLiteral);
      });

    });
  });

}));
