# electrodo_ca

ElectrodoCa - JavaScript client for electrodo_ca
Specification for REST API Electrodo CA
This SDK is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: io.swagger.codegen.languages.JavascriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/),
please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install electrodo_ca --save
```

##### Local development

To use the library locally without publishing to a remote npm registry, first install the dependencies by changing 
into the directory containing `package.json` (and this README). Let's call this `JAVASCRIPT_CLIENT_DIR`. Then run:

```shell
npm install
```

Next, [link](https://docs.npmjs.com/cli/link) it globally in npm with the following, also from `JAVASCRIPT_CLIENT_DIR`:

```shell
npm link
```

Finally, switch to the directory you want to use your electrodo_ca from, and run:

```shell
npm link /path/to/<JAVASCRIPT_CLIENT_DIR>
```

You should now be able to `require('electrodo_ca')` in javascript files from the directory you ran the last 
command above from.

#### git
#
If the library is hosted at a git repository, e.g.
https://github.com/YOUR_USERNAME/electrodo_ca
then install it via:

```shell
    npm install YOUR_USERNAME/electrodo_ca --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file, that's to say your javascript file where you actually 
use this library):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var ElectrodoCa = require('electrodo_ca');

var defaultClient = ElectrodoCa.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix['Authorization'] = "Token"

var api = new ElectrodoCa.AdminApi()

var username = "username_example"; // {String} The name that needs to download sertificate 

var body = new ElectrodoCa.Body2(); // {Body2} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.changeUserStatus(username, body, callback);

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8081/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ElectrodoCa.AdminApi* | [**changeUserStatus**](docs/AdminApi.md#changeUserStatus) | **PUT** /users/{username} | Approve or decline users
*ElectrodoCa.AdminApi* | [**deleteUser**](docs/AdminApi.md#deleteUser) | **DELETE** /users/{username} | method to delete user
*ElectrodoCa.AdminApi* | [**getListOfUsers**](docs/AdminApi.md#getListOfUsers) | **GET** /users | Returns list of users
*ElectrodoCa.AdminApi* | [**getUserInfo**](docs/AdminApi.md#getUserInfo) | **GET** /users/{username} | method to get info about user
*ElectrodoCa.GridApi* | [**gridCertification**](docs/GridApi.md#gridCertification) | **POST** /grid/{gridname} | Registration grid in system and get certificate and private key
*ElectrodoCa.UserApi* | [**createUser**](docs/UserApi.md#createUser) | **POST** /users | Create user
*ElectrodoCa.UserApi* | [**downloadCertificate**](docs/UserApi.md#downloadCertificate) | **GET** /users/{username}/download | download certificate
*ElectrodoCa.UserApi* | [**loginUser**](docs/UserApi.md#loginUser) | **POST** /users/login | Log user into the system


## Documentation for Models

 - [ElectrodoCa.Body](docs/Body.md)
 - [ElectrodoCa.Body1](docs/Body1.md)
 - [ElectrodoCa.Body2](docs/Body2.md)
 - [ElectrodoCa.GridCert](docs/GridCert.md)
 - [ElectrodoCa.InlineResponse200](docs/InlineResponse200.md)


## Documentation for Authorization


### Bearer

- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header

