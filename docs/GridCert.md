# ElectrodoCa.GridCert

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**city** | **String** |  | [optional] 


