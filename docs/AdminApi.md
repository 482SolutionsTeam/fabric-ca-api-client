# ElectrodoCa.AdminApi

All URIs are relative to *http://localhost:8081/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeUserStatus**](AdminApi.md#changeUserStatus) | **PUT** /users/{username} | Approve or decline users
[**deleteUser**](AdminApi.md#deleteUser) | **DELETE** /users/{username} | method to delete user
[**getListOfUsers**](AdminApi.md#getListOfUsers) | **GET** /users | Returns list of users
[**getUserInfo**](AdminApi.md#getUserInfo) | **GET** /users/{username} | method to get info about user


<a name="changeUserStatus"></a>
# **changeUserStatus**
> changeUserStatus(username, body)

Approve or decline users

Changing user status

### Example
```javascript
var ElectrodoCa = require('electrodo_ca');
var defaultClient = ElectrodoCa.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new ElectrodoCa.AdminApi();

var username = "username_example"; // String | The name that needs to download sertificate 

var body = new ElectrodoCa.Body2(); // Body2 | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.changeUserStatus(username, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| The name that needs to download sertificate  | 
 **body** | [**Body2**](Body2.md)|  | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser(username)

method to delete user

Delete User by name

### Example
```javascript
var ElectrodoCa = require('electrodo_ca');
var defaultClient = ElectrodoCa.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new ElectrodoCa.AdminApi();

var username = "username_example"; // String | The name of user that we will delete


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.deleteUser(username, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| The name of user that we will delete | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getListOfUsers"></a>
# **getListOfUsers**
> getListOfUsers(status)

Returns list of users

Returns a list of jsons

### Example
```javascript
var ElectrodoCa = require('electrodo_ca');
var defaultClient = ElectrodoCa.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new ElectrodoCa.AdminApi();

var status = "status_example"; // String | The status for choose list of users


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.getListOfUsers(status, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| The status for choose list of users | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserInfo"></a>
# **getUserInfo**
> InlineResponse200 getUserInfo(username)

method to get info about user

Get User info by name

### Example
```javascript
var ElectrodoCa = require('electrodo_ca');
var defaultClient = ElectrodoCa.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new ElectrodoCa.AdminApi();

var username = "username_example"; // String | The name of user


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getUserInfo(username, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| The name of user | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

