# ElectrodoCa.GridApi

All URIs are relative to *http://localhost:8081/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**gridCertification**](GridApi.md#gridCertification) | **POST** /grid/{gridname} | Registration grid in system and get certificate and private key


<a name="gridCertification"></a>
# **gridCertification**
> gridCertification(gridname, body)

Registration grid in system and get certificate and private key

Grid registration

### Example
```javascript
var ElectrodoCa = require('electrodo_ca');

var apiInstance = new ElectrodoCa.GridApi();

var gridname = "gridname_example"; // String | The name that needs to download sertificate 

var body = new ElectrodoCa.GridCert(); // GridCert | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.gridCertification(gridname, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gridname** | **String**| The name that needs to download sertificate  | 
 **body** | [**GridCert**](GridCert.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

