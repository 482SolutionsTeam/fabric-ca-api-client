# ElectrodoCa.InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**company** | **String** |  | [optional] 
**status** | **String** |  | [optional] 


