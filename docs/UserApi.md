# ElectrodoCa.UserApi

All URIs are relative to *http://localhost:8081/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UserApi.md#createUser) | **POST** /users | Create user
[**downloadCertificate**](UserApi.md#downloadCertificate) | **GET** /users/{username}/download | download certificate
[**loginUser**](UserApi.md#loginUser) | **POST** /users/login | Log user into the system


<a name="createUser"></a>
# **createUser**
> createUser(body)

Create user

User registration

### Example
```javascript
var ElectrodoCa = require('electrodo_ca');

var apiInstance = new ElectrodoCa.UserApi();

var body = new ElectrodoCa.Body(); // Body | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.createUser(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body**](Body.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

<a name="downloadCertificate"></a>
# **downloadCertificate**
> downloadCertificate(username)

download certificate



### Example
```javascript
var ElectrodoCa = require('electrodo_ca');
var defaultClient = ElectrodoCa.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new ElectrodoCa.UserApi();

var username = "username_example"; // String | The name that needs to download certificate 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.downloadCertificate(username, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| The name that needs to download certificate  | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

<a name="loginUser"></a>
# **loginUser**
> loginUser(body)

Log user into the system



### Example
```javascript
var ElectrodoCa = require('electrodo_ca');

var apiInstance = new ElectrodoCa.UserApi();

var body = new ElectrodoCa.Body1(); // Body1 | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.loginUser(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body1**](Body1.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/json

