/*
 * Electrodo CA
 * Specification for REST API Electrodo CA
 *
 * OpenAPI spec version: 1.0.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.15
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/GridCert'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/GridCert'));
  } else {
    // Browser globals (root is window)
    if (!root.ElectrodoCa) {
      root.ElectrodoCa = {};
    }
    root.ElectrodoCa.GridApi = factory(root.ElectrodoCa.ApiClient, root.ElectrodoCa.GridCert);
  }
}(this, function(ApiClient, GridCert) {
  'use strict';

  /**
   * Grid service.
   * @module api/GridApi
   * @version 1.0.0
   */

  /**
   * Constructs a new GridApi. 
   * @alias module:api/GridApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the gridCertification operation.
     * @callback module:api/GridApi~gridCertificationCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Registration grid in system and get certificate and private key
     * Grid registration
     * @param {String} gridname The name that needs to download sertificate 
     * @param {module:model/GridCert} body 
     * @param {module:api/GridApi~gridCertificationCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.gridCertification = function(gridname, body, callback) {
      var postBody = body;

      // verify the required parameter 'gridname' is set
      if (gridname === undefined || gridname === null) {
        throw new Error("Missing the required parameter 'gridname' when calling gridCertification");
      }

      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling gridCertification");
      }


      var pathParams = {
        'gridname': gridname
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = null;

      return this.apiClient.callApi(
        '/grid/{gridname}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
